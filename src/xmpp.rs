use std::str::FromStr;
use futures::{Future, Stream};
use xmpp_parsers::{
    BareJid, Jid,
    message::MessageType,
};
use xmpp::{ClientBuilder, ClientType, Event, Agent as XMPPAgent};

#[derive(Clone)]
pub struct XMPPBot {
    agent: XMPPAgent,
    muc_jid: BareJid,
}

impl XMPPBot {
    pub fn new<'a>(jid: &'a str, password: &'a str, muc_jid: &'a str) -> (Box<dyn Future<Item = (), Error = ()> + 'a>, Self) {
        let muc_jid: BareJid = match BareJid::from_str(muc_jid.clone()) {
            Ok(jid) => jid,
            Err(err) => panic!("MUC Jid invalid: {:?}", err),
        };

        let (agent, stream) = ClientBuilder::new(jid.clone(), password)
            .set_client(ClientType::Bot, "xmpp-rs")
            .set_website("https://gitlab.com/xmpp-rs/tokio-webhook2muc")
            .set_default_nick("bot")
            .build()
            .unwrap();

        let handler = {
            let mut agent = agent.clone();
            let muc_jid = muc_jid.clone();
            let jid = jid.clone();
            stream.map_err(|_| ()).for_each(move |evt: Event| {
                match evt {
                    Event::Online => {
                        info!("XMPP client now online at {}", jid);
                        agent.join_room(muc_jid.clone(), None, None, "en", "Your friendly hook bot.");
                    },
                    Event::Disconnected => {
                        info!("XMPP client disconnected");
                    },
                    Event::RoomJoined(jid) => {
                        info!("Entered MUC {}", jid);
                    },
                    Event::RoomLeft(jid) => {
                        info!("Left MUC {}", jid);
                    },
                    _ => (),
                }
                Ok(())
            })
            .map(|_| ())
        };

        (Box::new(handler), XMPPBot { agent, muc_jid: muc_jid.clone() })
    }

    pub fn send_room_text(&mut self, text: String) {
        self.agent.send_message(Jid::Bare(self.muc_jid.clone()), MessageType::Groupchat, "en", &text);
    }
}
